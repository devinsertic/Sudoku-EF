import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.Scanner;
import java.io.File;       import java.io.FileNotFoundException;
/**
 * Eine FileReader, der unsere Dateien (wie Einstellungen einliest) und formatiert, sodass diese im Programm verwendet werden k�nnen
 */
public class FileReader extends Actor
{
    private String[] loadedSettings = new String[2]; // geladene Einstellungen
    private String settingsFile; // Name der Txt, worin alles gespeichert wird
    
    /**
     * Konstruktor
     * 
     * @param pSettingsFile Name der Datei
     */
    public FileReader(String pSettingsFile){
        settingsFile = pSettingsFile;
    }
    
    /**
     * Act - do whatever the fileReader wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }
    
    /**
     * Eigentliche Read-Methode - liest die Datei ein
     * 
     * @return gibt eine Liste mit den gelesenen Inhalt (Einstellungen) zur�ck
     */
    public String[] readFile(){
        Scanner scan = null;
        try{
            scan = new Scanner(new File(settingsFile));
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
        }
        
        int counter = 0;
        while(scan.hasNext()){
            loadedSettings[counter] = scan.nextLine();
            counter++;
        }
        scan.close();
        
        return loadedSettings;
    }
}
