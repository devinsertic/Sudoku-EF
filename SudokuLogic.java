import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
import java.lang.Math;
import java.util.Arrays;
import java.util.HashMap;
/**
 * Diese Klasse enth�lt Logik, sowie Methoden, die sowohl im SudokuSolver, 
 * als auch im SudokuGame gebraucht werden
 * 
 * Es ist also die Hauptklasse und das Herzst�ck des Spiels
 * 
 * @author Devin S. & Bene R, 
 * @version (a version number or a date)
 */
public class SudokuLogic extends World
{
    public final int buffer = 0; // buffer ben�tigt, da durch 81/2 40.5 gerundet wird. Dies wird 9 mal getan, also 9*0.5 gerundet 5. Ohne Linie zu gro�.
    public final int beautifier = 1; // um die Umrandung ein st�ck n�her an das Gitter zu bringen. Nicht notwendig aber sch�ner.
    private final String settingsFileName = "settings";
   
    
    public int squareSize = 9; // Anzahl der K�stchen einer Row und einer Collumn -> Zusammen 9x9 = 81 K�stchen
    public int imageSize = 40; // Pixelgr��e eines K�stchen - width & height

    public int thickness = 5; // Dicke der Grid Umrandung
    public GreenfootImage backgroundBorderImage = new GreenfootImage(900, 600); // Bild, worauf sich die Umrandung des Grids befindet

    public String fieldColor; // Farbe eines K�stchens
    public String borderColor; // Farbe der Umrandung

    private int sudokuCounter = 0;

    public String currentGameDifficulty = "";
    
    private int[][] grid = new int[9][9]; // Grid 9x9 -> Sudoku Feld/Netz
    private int[][] generatedGridEmpty = new int[9][9];
    private int[][] generatedGridSolved = new int[9][9];
    
    private HashMap<String, Integer> difficultyMap = new HashMap<String,Integer>(); // Map in der den verschiedenen Schwierigkeiten, werte zugewisen werden (wieviele freie felder es geben soll))
    
    public static final int GAME = 0; // Bezeichnung f�r den Ort von dem aus etwas augerufen wird -> SudokuGame
    public static final int SOLVER = 1; // Bezeichnung f�r den Ort von dem aus etwas augerufen wird -> Sudoku Solver
    
    private GreenfootSound victory = new GreenfootSound("Victory_Sound.mp3"); // Victory Sound -> Wird gespielt, wenn nach �berpr�fung alles richtig ist
    
    public boolean isSolvedByAlgorithm = false; // Zeigt, ob die L�sung durch den Algorihmus entstanden ist
    
    /**
     * Konstruktor der Klasse SudokuLogic
     */
    public SudokuLogic()
    {    
        super(900, 600, 1); // Erstellt eine Welt mit 900 x 600 pixeln
        saveSettingsLocal(); // Lie�t bereits gespeicherte Einstellungen ein
        setPaintOrder(SudokuNumber.class, Field.class); // Setzt die Paint-Order so, dass Felder nicht vor den Nummern sind, sodass sie zusehen sind
        
        initDifficultyMap();
    }

    // Hauptmethoden (public)
    
    /**
     * K�mmert sich darum, dass ein Bild gesetzt wird, hinter dem Sudoku Feld, worauf das Sudoku Feld Umrandet wird um innere 3x3 Felder farblich hervor zu heben
     */
    public void setColoredGridBorder(){
        List<Field> allFields = getObjects(Field.class); //Alle aktuellen Felder

        //Anfangspunkte f�r den ersten Strich - mithilfe von diesen Punkten, werden weitere berechnet

        int LineOneX=0; int LineOneY=0;       //Koordinaten Punkt 1
        int LineTwoX=0; int LineTwoY=0;       //Koordinaten Punkt 2

        //Iterriert durch alle Felder und speichert die Koordinaten in den Variablen oben
        for(Field field : allFields){
            if(field.id == 1){
                LineOneX = field.getX() - imageSize/2 - beautifier; // ImageSize/2 da getX() die Mitte des Objektes wiedergiebt und die x/y Koordinaten au�en gesucht sind
                LineOneY = field.getY() - imageSize/2;
            }
            else if(field.id == squareSize){
                LineTwoX= field.getX()+ imageSize/2 - beautifier; // beautifier auf grund von ungenauigkeiten beim rechnen -> aufrundung auf int
                LineTwoY= field.getY()- imageSize/2;    
            }
        }

        backgroundBorderImage.setColor(stringToColor(borderColor)); // setzt farbe der Striche

        // Unten definierte grid enthalten jeweils die X bzw. Y Koordinaten der Punkte, wo der Strich durch soll.
        // Immer zwei X bzw Y Werte nebeneinader f�r zusammengeh�rige Punkte 

        //above                 left                 down                      right
        int[] outterLineXs = {LineOneX, LineTwoX, LineOneX, LineOneX, LineOneX, LineTwoX, LineTwoX, LineTwoX};
        //above                 left                                                                         down                                                                      right    
        int[] outterLineYs = {LineOneY, LineTwoY, LineOneY, LineTwoY+(squareSize*imageSize-buffer), LineTwoY+(squareSize*imageSize-buffer), LineTwoY+(squareSize*imageSize-buffer),LineTwoY+(squareSize*imageSize-buffer), LineTwoY};

        int[] innerLinesXs = {LineOneX,LineTwoX,LineOneX, LineTwoX, LineOneX+(squareSize/3*imageSize),LineOneX+(squareSize/3*imageSize),LineOneX+(squareSize/3*imageSize)*2,LineOneX+(squareSize/3*imageSize)*2};

        int[] innerLinesYs = {LineOneY+(squareSize/3*imageSize), LineTwoY+(squareSize/3*imageSize), LineOneY+((squareSize/3*imageSize)*2), LineTwoY+((squareSize/3*imageSize)*2),LineOneY,LineTwoY+(squareSize*imageSize-buffer), LineOneY, LineTwoY+(squareSize*imageSize-buffer)};

        // �u�ere Linien des Sudokus Feldes
        for(int i = 0; i<outterLineXs.length-1; i+=2){
            drawGridLine(outterLineXs[i], outterLineYs[i], outterLineXs[i+1], outterLineYs[i+1]);
        }

        //Innere Linien des Sudoku Feldes
        for(int i = 0; i<innerLinesXs.length-1; i+=2){ // 2-Schritte um immer 2 P�rchen zu erhalten
            drawGridLine(innerLinesXs[i], innerLinesYs[i], innerLinesXs[i+1], innerLinesYs[i+1]);
        }

        getBackground().drawImage(backgroundBorderImage, 0, 0); // zeichnet die Umrandungen auf die Welt
    }

    /**
     * Baut aus den einzelnen Field-Obejekten ein Sudoku-Gitter
     * 
     * @param firstPosX X-Koordinate f�r das Erste Feld, welches gesetzt werden soll
     * @param firstPosY Y-Koordinate f�r das Erste Feld, welches gesetzt werden soll
     */
    public void buildGrid(int firstPosX, int firstPosY, int buildPlace){
        int posX = 0;
        int posY = 0;
        for (int row=0; row<9; row++) {
            for (int col=0; col<9; col++) {
                // new Field(this, row, col,...)
                //grid[row][col];
                //id = 9*row + col;
                //row=id/9;
                //col=id%9;
            }
        }
        
        for (int i=0;i<squareSize*squareSize;i++)
        {
            if(posX == squareSize){
                posX = 0;
                posY--;
            }
            
            if(buildPlace == 1){
                Field field = new Field(this, i, posX, -posY, squareSize, "filled-"+fieldColor+".png"); // Setzt das Feld an seine berechnete Position
                addObject(field, firstPosX+(posX*(imageSize)), firstPosY-(posY*(imageSize)));
            }
            else{
                PlayField field = new PlayField(this, i, posX, -posY, squareSize, "filled-"+fieldColor+".png"); // Setzt das Feld an seine berechnete Position
                addObject(field, firstPosX+(posX*(imageSize)), firstPosY-(posY*(imageSize)));
            }
            posX++;
        }
    }
    
    // Funktionene f�r Buttons und weitere Funktionen, die von au�erhalb ben�tigt werden
    
        /**
     * Gibt das ganze Array/Grid/Sudoku zur�ck
     * 
     */
    public int[][] getArray(){
        return grid;
    }
    
    /**
     * Gibt das fertige Sudoku zur�ck
     */
    public int[][] getSolvedArray() {
        return generatedGridSolved;
    }
    
    
    /**
     * Gibt f�r eine gewisse Schwierigkeit, die Anzahl an freien Feldern aus der HashMap zur�ck
     * 
     * @param difficultyKey Ein String der die Schwierigkeit beschreibt und f�r den wir den Wert aus der HashMap haben wollen
     */
    public int getDifficultyValue(String difficultyKey) {
        return difficultyMap.get(difficultyKey);
    }
    
    /**
     * Setzt einen Wert an eine besimmte Stelle ins grid
     * 
     * @param row Row des Sudoku Feldes
     * @param col Column des Sudoku Feldes
     * @param val Value/Nummer die eingesetz werden soll
     */
    public void setArray(int row, int col, int val){
        grid[row][col] = val;      
    }

    /**
     * Entfernt alles Zahlen aus dem Sudoku, sowohl in der UI, als auch im grid
     * 
     **/
    public void clearSudoku() {
        List<Field> allFields = getObjects(Field.class);
        removeAllNumbersFromUI();
        grid = new int[9][9];
    }
    
    /**
     * Pr�ft, ob falsche Werte vorliegen und markiert diese farbig
     */
    public void checkWrong() {
        List<Field> allFields = getObjects(Field.class);
        int mistakes = 0;

        for(Field d: allFields){
            if(d.containsUserNum /* || d.number == 0 */) {
                if(grid[d.row-1][d.col-1] != generatedGridSolved[d.row-1][d.col-1]) {
                    mistakes++;
                    d.setFieldWrong();
                }
            }
            if(d.number == 0) {
                mistakes++;
            }
        }
        
        if(mistakes == 0 && !isSolvedByAlgorithm){
            victory.play();
            for(Field d: allFields) {
                d.setFieldVictory();
            }
        }
        else if(isSolvedByAlgorithm) {
            for(Field d: allFields) {
                d.setFieldVictory();
            }
        }
    }
    
    /**
     * L�st das Grid, welches vom User bearbeitet wird, in SudokuGame
     * 
     */
    public void solvePlayerGrid() {
        resetSudoku();
        isSolvedByAlgorithm = true;
        printGivenSudokuGridOnUI(getSolvedArray());
        removeRedFields();
    }
    
    /**
     * Da, die Kontrolle, auf dem Attribut Number der Felder basiert, m�ssen nicht nur grid und UI geupdated werden, sonder auch diese Attribute, dies wird jedes mal getan
     * , wenn ein neues Grid generiert wird.
     */
    public void setRightAttributNumberInField(){
        List<Field> allFields = getObjects(Field.class); // Hole alle Felder aus dem Grid
        for(Field d: allFields){
            d.number = grid[d.row-1][d.col-1];// Ordne jedem Feld aus dem Array seine Zahl zu und setze diese Zahl, als Number Object auf das Feld
            if(d.number == 0) {
                d.setContainsUserNum(false);
            }
        }
    }
    
    /**
     * Entfernt nur Nutzereingaben aus dem Sudoku
     */
    public void resetSudoku() {
        grid = Arrays.stream(generatedGridEmpty).map(int[]::clone).toArray(int[][]::new);
        removeAllNumbersFromUI();
        setRightAttributNumberInField();
        removeRedFields();
    }
    
    /**
     * Entfernt die roten Felder, die bei der �berpr�fung entstehen wieder
     */
    public void removeRedFields() {
        List<Field> allFields = getObjects(Field.class);

        for(Field d: allFields){ 
            d.setGetNumber(grid[d.row-1][d.col-1]); // Ordne jedem Feld aus dem Array seine Zahl zu und setze diese Zahl, als Number Object auf das Feld
            if(d.isWrong && !isSolvedByAlgorithm) {
                d.undoFieldWrong();
            }
        }
    }
    
    
    // Algorithmus zum L�sen des Sudokus - Backtracking (Recursion)
    /**
     * Hauptmethode des Backtracking Algorithmus - l�st das Sudoku und gibt die L�sung aus, durch ausf�llen des Grids auf der Welt
     */
    public void printSolvedGridOnUI(){
        if(solve(0, 0)){ // Wenn der Algorithmus fertig ist, also gel�st
            removeAllNumbersFromUI(); // Entferne Nummern aus der UI
            List<Field> allFields = getObjects(Field.class); // Hole alle Felder aus dem Grid
            for(Field d: allFields){
                d.setGetNumber(grid[d.row-1][d.col-1]); // Ordne jedem Feld aus dem Array seine Zahl zu und setze diese Zahl, als Number Object auf das Feld
            }
        }
        else{ // Falls das Sudoku nicht L�sbar ist
            System.out.println("Nicht l�sbar"); // Fehlermeldung
        }
    }

    /**
     * Printet ein �bergebenes Array auf die Welt
     */
    public void printGivenSudokuGridOnUI(int[][] pGrid){
        removeAllNumbersFromUI(); // Entferne Nummern aus der UI
        List<Field> allFields = getObjects(Field.class); // Hole alle Felder aus dem Grid
        for(Field d: allFields){
            d.setGetNumber(pGrid[d.row-1][d.col-1]); // Ordne jedem Feld aus dem Array seine Zahl zu und setze diese Zahl, als Number Object auf das Feld
        }
        
        grid = Arrays.stream(generatedGridSolved).map(int[]::clone).toArray(int[][]::new);

    }
    
    // Main Funktion f�r den Generierungsalgorithmus
    
    /**
     * Generiert ein neues/zuf�lliges Sudoku Puzzel indem:
     * 1. Die diagonalen 3x3 Boxen mit random Nummern bef�llt werden -> Diagonal, da es dadurch keine �berschneidungen in row und column geben kann (Somit nur eine Regeln die zu beachten gilt)
     * 2. Das ganze grid mit Zahlen f�llt -> durch l�sen
     * 3. Einzelne Werte entfernt und pr�ft ob es noch l�sbar ist
     * 
     * @param numbersToBeDeleted Anzahl an freien Feldern
     */
    public void generateGrid(int numbersToBeDeleted) {
        grid = new int[9][9];
        // first box
        fillDiagonalBoxes(0, 2);
        
        // second box
        fillDiagonalBoxes(3, 5);
        
        // third box
        fillDiagonalBoxes(6, 8);
        
        
        
        if(solve(0,0)){
            // Nun gel�st, also:
            generatedGridSolved = Arrays.stream(grid).map(int[]::clone).toArray(int[][]::new);

            
            removeNumbers(numbersToBeDeleted); // l�sche eine Zahl
            
            generatedGridEmpty = Arrays.stream(grid).map(int[]::clone).toArray(int[][]::new);

            List<Field> allFields = getObjects(Field.class); // Hole alle Felder aus dem Grid
            for(Field d: allFields){
                d.setGetNumber(grid[d.row-1][d.col-1]); // Ordne jedem Feld aus dem Array seine Zahl zu und setze diese Zahl, als Number Object auf das Feld
            }
        }
        else {
            System.out.println("Fehler"); // Fehlermeldung
        }
    }

    // Generator Helper functions
    /**
     * F�llt eine der 3x3 Boxen mit zuf�lligen Weren
     * 
     * @param from Anfangswert der Box
     * @param to Endwert der Box
     */
    private void fillDiagonalBoxes(int from, int to) {
        to++;
        for(int row=from; row<to; row++) {
            for(int col=from; col<to; col++){
                int number;
                do {
                    number = Greenfoot.getRandomNumber(9)+1;
                } while (inDiagonalBox(number, from, to));
                
                grid[row][col] = number;
            }
        }
    }
    
    /**
     * Pr�ft ob es denn wert bereits in der 3x3 Box gibt
     * 
     * @param from Anfangswert der Box
     * @param to Endwert der Box
     * @param number Zu �berpr�fende Nummer
     */
    private boolean inDiagonalBox(int number, int from, int to){
        for(int row=from; row<to; row++) {
            for(int col=from; col<to; col++) {
                if(grid[row][col] == number){
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Entfernt solange Zahlen aus dem Grid, bis gen�gend entfernt wurden
     * 
     * @param num Nummer der zu entfernenden Zahlen
     */
    private void removeNumbers(int num) {
        int removedNumbers = 0;
        int row;
        int col;
        do {
            row = Greenfoot.getRandomNumber(9);
            col = Greenfoot.getRandomNumber(9);
            
            if(grid[row][col] == 0) {
                continue; 
            }
            
            if(removeNumber(row, col)){
                removedNumbers++;
            }
        } while (removedNumbers != num);
    }
    
    /**
     * Pr�ft, ob das Sudoku noch l�sbar w�re, wenn die gegebene Zahl dort entfernt wird
     * 
     * @param row Row der zu entfernenden Zahl
     * @param col Col der zu entfernenden Zahl
     */
    private boolean removeNumber(int row, int col) {
        int temp = grid[row][col];
        
        grid[row][col] = 0;
        if(checkSolve(row, col)){
            return true;
        }
        
        grid[row][col] = temp;
        return false;
    }
    

    
    
    // Helperfunctions (private)
    
    // F�r UI
    /**
     * zeichnet einen Strich mit einer gewissen Dicke durch zwei gegebene Punkte - Verwendung f�r Gitterzeichnung
     * 
     * @param firstX X-Wert f�r den ersten Punkt (p1)
     * @param firstY Y-Wert f�r den ersten Punkt (p1)
     * 
     * @param secondX X-Wert f�r den zweiten Punkt (p2)
     * @param secondY Y-Wert f�r den zweiten Punkt (p2)
     */
    private void drawGridLine(int firstX, int firstY, int secondX, int secondY){
        // Zeichnet eine weitere linien rechts von der Urspr�nglichen
        for(int i=0;i<thickness/2;i++){
            for(int j=0;j<thickness/2;j++){
                backgroundBorderImage.drawLine(firstX+i,firstY+j,secondX+i,secondY+j); // Zeichnet die Linie
            }
        }

        // Zeichnet eine weitere Linie links von der �rspr�nglichen
        for(int i=0;i<thickness/2;i++){
            for(int j=0;j<thickness/2;j++){
                backgroundBorderImage.drawLine(firstX-i,firstY-j,secondX-i,secondY-j); // Zeichnet die Linie
            }
        }
    }

    /**
     * @param pColorName Name einer Farbe als String
     * @return gibt eine greenfoot.color zur�ck mit dem Namen des Strings 
     */
    private Color stringToColor(String pColorName){
        if(pColorName.equals("orange")){
            return Color.ORANGE; // Orange
        }
        else if(pColorName.equals("cyan")){
            return Color.CYAN; // Cyan
        }
        else if(pColorName.equals("red")){
            return Color.RED; // Rot
        }
        else if(pColorName.equals("black")){
            return Color.BLACK; // Schwarz
        }
        return Color.BLACK; // Falls irgendwie eine ung�ltige -> Standartfarbe Schwarz
    }

    /**
     * Lie�t die Einstellungen aus dem Settingsfile ein und speichert sie in einem Attribut, um sp�ter damit weiterarbeiten zu k�nnen
     */
    private void saveSettingsLocal(){
        FileReader fileReader = new FileReader(settingsFileName); // Neuer FileReader, f�r die Settings datei
        String[] settings = fileReader.readFile(); // Datei wird eingelsen

        fieldColor = settings[0].split("&")[1]; // Da settings an der ersten Stelle die Fieldsettings enth�lt
        borderColor = settings[1].split("&")[1]; // Zweite stelle enth�lt Border Settings
    } 

    /**
     * Entfernt die Number Objekte von der UI nzw. vom Grid
     * 
     * Wird gebraucht um bei der L�sung �berschneidungen von UI-Elementen zu vermeiden
     */
    private void removeAllNumbersFromUI(){
        List<SudokuNumber> numbers = getObjects(SudokuNumber.class); // Lister aller Number Objekte -> Zahlen die eingetragen wurden
        for(SudokuNumber num: numbers){ // Itterarion durch diese Liste von Nummern
            removeObject(num); // Entfernt diese aus der Welt
        }
    }

    // Debug Funktionen
    
    public void printLine() {
        System.out.println(""+  sudokuCounter + " -------------------");       
    }
    
    public void printSudoku() {
        sudokuCounter++;
        printLine();
        for (int i=0; i<9; i++) {
            for (int j=0; j<9; j++) {
                System.out.print(" " + grid[i][j]);
            }
            System.out.println();
        }
    }
    
    // F�r Solve-Algorithmus
    
    /**
     * Initalisiert die DifficultyMap, die die Schwierigkeitsgrade enth�lt
     * 
     * Key = Bezeichnung
     * Value = zu entfernende Anzahl an Nummern
     */
    private void initDifficultyMap() {
        difficultyMap.put("easy", 30);
        difficultyMap.put("middle", 40);
        difficultyMap.put("hard", 50);
        difficultyMap.put("hardcore", 60);
    }
    
    /**
     * Pr�ft ob der Zug, mit den �bergebenen Werten g�ltig ist
     * 
     * @param row Reihe des Zuges, welcher probiert wird
     * @param col Spalte des Zuges, welcher probiert wird
     * @param val Zahl, die in das Felde eingegeben wurde und getestet wird
     * 
     * @return boolean Gibt True zur�ck, wenn der Zug funktioniert und False wenn nicht
     */
    public boolean isSafe(int row, int col, int val){
        printSudoku();
        for(int i = 0; i < 9; i++) {
            if(grid[i][col] == val){
                return false;
            }// Pr�ft Reihe
            if(grid[row][i] == val){
                return false; 
            }// Pr�ft Spalte
            if(grid[3 * (row / 3) + i / 3][3 * (col / 3) + i % 3] == val){
                return false;
            } // Pr�ft den 3x3 Block
        }
        return true; // Falls keine Regel verletzt wurde, gebe True zur�ck
    }

    /**
     * Eigentlicher Backtracking Algorithmus - Hauptmethode
     * 
     * L�st ein Sudoku
     * 
     * @param row Start-Reihe
     * @param col Start-Zeile
     */
    private boolean solve(int row, int col) {
        for(int i = row; i < grid.length; i++, col = 0) { // Iterration �ber alle Reihen - Rows
            for(int j = col; j < grid[0].length; j++) { // Iterration �ber alle Zeilen in Reihe
                if(grid[i][j] == 0) { // Wenn Feld leer, keine Zahl
                    for(char c = 1; c <= 9; c++) {// Versuche alle Zahlen - Iterration von 1-9
                        if(isSafe(i, j, c)) { // Wenn Zahl in Reihe und Zeile einsetzbar - gegen keine Regln
                            grid[i][j] = c; // Setzt Zahl in das Feld
                            if(solve(i, j+1)) // geht weiter um Zu pr�fen ob die Zahl entg�ltig richtig ist
                                return true; // Wenn ja, true
                            else
                                grid[i][j] = 0; // Wenn nein, backtracke - gehe zur�ck und probiere es weiter
                        }
                    }
                    return false; // Zahl passt nicht ins feld
                }
            }
        }
        return true; // Algorithmus abeschlossen, Feld gel�st
    }
    
    
    /**
     * Wie solve, nur das die Methode nicht in das grid schreibt
     * Stattdessen pr�ft sie nur, ob das Sudoku noch eine L�sung hat, wenn ein Wert entfernt wurde
     * 
     * Checkt ein Sudoku nach L�sung
     * 
     * @param row Start-Reihe
     * @param col Start-Zeile
     */
    private boolean checkSolve(int row, int col) {
        int[][] tempGrid = Arrays.stream(grid).map(int[]::clone).toArray(int[][]::new);
        for(int i = row; i < tempGrid[0].length; i++, col = 0) { // Iterration �ber alle Reihen - Rows
            for(int j = col; j < tempGrid[0].length; j++) { // Iterration �ber alle Zeilen in Reihe
                if(tempGrid[i][j] == 0) { // Wenn Feld leer, keine Zahl
                    for(char c = 1; c <= 9; c++) {// Versuche alle Zahlen - Iterration von 1-9
                        if(isSafe(i, j, c)) { // Wenn Zahl in Reihe und Zeile einsetzbar - gegen keine Regln
                            tempGrid[i][j] = c; // Setzt Zahl in das Feld
                            if(checkSolve(i, j+1)) // geht weiter um Zu pr�fen ob die Zahl entg�ltig richtig ist
                                return true; // Wenn ja, true
                            else
                                tempGrid[i][j] = 0; // Wenn nein, backtracke - gehe zur�ck und probiere es weiter
                        }
                    }
                    return false; // Zahl passt nicht ins feld
                }
            }
        }
        return true; // Algorithmus abeschlossen, Feld gel�st
    }
}
