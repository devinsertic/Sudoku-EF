import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List; // Listen
/**
 * Welt in der Soduku gespielt werden kann - benutzt Methoden der SudokuLogic, um die Logik und UI zu implementieren
 * 
 * @author Devin S. - Benedikt R. 
 * 
 */
public class SudokuGame extends SudokuLogic
{   
    private float beginTime = System.currentTimeMillis();
    private String difficulty; // Schwierigkeit des Sudokuspiels
    /**
     * Konstruktor des Sudokuspiels
     * 
     */
    public SudokuGame(String gameDifficulty)
    {    
        super();
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        difficulty = gameDifficulty; // �bergebener Parameter wird zum Attribut
        prepare(); // baut UI
    }
    
    /**
     * Baut das Spielfeld (UI)
     * 
     */
    private void prepare(){
        getBackground().setColor(Color.BLACK); // Setzt hintergrund Schriftfarbe der obenstehenden Nachticht auf Schwarz
        getBackground().setFont(new Font("OptimusPrinceps", false, false , 17));
        
        buildGrid(getWidth()/2 - imageSize*(squareSize/2), getHeight()/2 - imageSize*(squareSize/2), SudokuLogic.GAME); // Baut das Sudokunetz
        setColoredGridBorder(); // Setzt die Umrandung des Suokus
        worldButton button = new worldButton("back","TitleScreen"); // Zur�ckbutton
        addObject(button, 75, 550);
        generateGrid(getDifficultyValue(difficulty));
        
        // Timer
        
        
        // Buttons
        
        /*
         * l�schen x
         * neues Spiel
         * �berpr�fen
         * l�sen x
         * Hinweis
         */
        
        // Clear Button
        functionButton button2 = new functionButton("clearGameGrid", "Delete");
        addObject(button2, 800 , 290);
            // Clear String
        getBackground().setFont(new Font("OptimusPrinceps", false, false , 17)); // Setzt Schriftart der Nachricht
        getBackground().drawString("L�schen", 770, 335);
        
        // Solve Button
        functionButton button1 = new functionButton("solveGameGrid","l�sen"); // Funktionsbutton um �bergebenes Sudoku zu l�sen
        addObject(button1,getWidth()/3, 550); // f�gt button der Welt hinzu
        
        // Check Button
        
        functionButton button3 = new functionButton("checkGameGrid","�berpr�fen"); // Funktionsbutton um �bergebenes Sudoku zu l�sen
        addObject(button3,getWidth()/2, 550); // f�gt button der Welt hinzu
        
        // New Game Button
        functionButton button4 = new functionButton("newSudoku","neuesSpiel"); // Funktionsbutton um �bergebenes Sudoku zu l�sen
        addObject(button4,getWidth()/2 + 160, 550); // f�gt button der Welt hinzu
    }
}