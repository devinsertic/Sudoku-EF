    import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot und MouseInfo)
    
    /**
    * Rules ist die Welt, in der das Spielprinzip und die dazugeh�rigen Regeln
    * erkl�rt werden
    */
    public class Rules extends World
    {
    
    /**
     * Konstruktor f�r Objekte der Klasse Rules
     * 
     */
    public Rules()
    {    
        // Erstellt eine neue Welt mit 600x400 Zellen und einer Zell-Gr��e von 1x1 Pixeln.
        super(900, 600, 1);
        prepare();
    }
    
    private void prepare() {
        getBackground().setColor(Color.BLACK); // Setzt hintergrund Schriftfarbe der obenstehenden Nachticht auf Schwarz
        getBackground().setFont(new Font("OptimusPrinceps", true, false , 26)); // Setzt Schriftart der Nachricht
        
        getBackground().drawString("1 Ziel, 3 Regeln", 60, 80); // F�gt die Nachricht der Welt hinzu
        
        getBackground().setFont(new Font("OptimusPrinceps", false, false , 17)); // Setzt Schriftart der Nachricht
    
        getBackground().drawString("Ziel des Spiels ist es, alle K�stchen mit Ziffern von 1 - 9 zu f�llen.", 60, 150); // F�gt die Nachricht der Welt hinzu
    
        getBackground().drawString("Hierbei gibt es jedoch drei Regeln:", 60, 185); // F�gt die Nachricht der Welt hinzu
        
        getBackground().drawString("1. In jeder           darf nur jeweils einmal jede Ziffer vorkommen", 60, 230); // F�gt die Nachricht der Welt hinzu
    
        getBackground().drawString("2. Dies gilt auch f�r jede ", 60, 270); // F�gt die Nachricht der Welt hinzu
        
        getBackground().drawString("3. Und auch f�r jeden           mit 3x3 K�stchen.", 60, 310); // F�gt die Nachricht der Welt hinzu
        
        // Reihe farbig - Orange
        getBackground().setColor(Color.ORANGE); // Setzt hintergrund Schriftfarbe der obenstehenden Nachticht auf Schwarz
        getBackground().drawString("Reihe", 140, 230); // F�gt die Nachricht der Welt hinzu
    
        // Spaltig farbig - Gr�n
        getBackground().setColor(Color.GREEN); // Setzt hintergrund Schriftfarbe der obenstehenden Nachticht auf Schwarz
        getBackground().drawString("Spalte", 250, 270); // F�gt die Nachricht der Welt hinzu
    
        // Block farbig - Blau
        getBackground().setColor(Color.BLUE); // Setzt hintergrund Schriftfarbe der obenstehenden Nachticht auf Schwarz
        getBackground().drawString("Block", 227, 310); // F�gt die Nachricht der Welt hinzu
    
        // Erkl�rungsbild
        
        GreenfootImage titel = new GreenfootImage("help.jpg"); // Titelbilschirm wird erstellt
        getBackground().drawImage(titel, 500, 330); // und hinzugef�gt
    
        // Zur�ck-Button
        worldButton button = new worldButton("back","TitleScreen");
        addObject(button, 75, 550);
    }
}