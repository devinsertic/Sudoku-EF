import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Ein Button, der die Einstellungen �ffnet
 */
public class Settings extends Button
{
    /**
     * Act - do whatever the Settings wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
        checkClick();
    }    
    
    /**
     * Pr�ft, ob das Objekt geklickt
     */
    private void checkClick(){
        if(Greenfoot.mouseClicked(this)){
            // Settings �ffnen
            SettingsMenue world = new SettingsMenue();
            Greenfoot.setWorld(world);
        }
    }
}
