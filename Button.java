import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Hierbei handelt es sich um eine Oberklasse alle Buttons
 * Um Funktionen, die alle Buttons haben, auszulagern
 */
public class Button extends Actor
{
    private boolean mouseOver = false; // Aktueller Status zum Hover Event
    
    public GreenfootSound click = new GreenfootSound("click_sound.mp3"); // Klick Sound f�r jeden Button
    public GreenfootSound clickBack = new GreenfootSound("click2.mp3"); // Klick Sound f�r zur�ck Button
    public GreenfootSound trash = new GreenfootSound("Papierkorb.mp3"); // Klick Sound f�r L�schbutton
    
    /**
     * Act - do whatever the ControlElements wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }
    
    public Button() {
        click.setVolume(50);
        clickBack.setVolume(50);
    }
    
    /**
     * Pr�ft ob die Maus sich auf dem Objekt befindet -> pr�ft nach "Hover" Event
     * 
     * @param designation Bezeichnung des Button Bildes
     */
    public void checkHovering(String designation){
        
        if(!mouseOver && Greenfoot.mouseMoved(this)){
            setImage(designation + "-hover.png");
            mouseOver = true;
        }
        if(mouseOver && Greenfoot.mouseMoved(null) && !Greenfoot.mouseMoved(this)){
            setImage(designation + ".png");
            mouseOver = false;
        }
    }
}
