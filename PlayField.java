import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class playField here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class PlayField extends Field
{
    /**
     * Feld, welches nur bei dem normalen Sudoku-Spiel zum Einsatz kommt, da in diesem auch falsche Eingaben m�glich sein sollen
     */
    public void act() 
    {
        clickManager(false); // 
        setGetNumber(0);
        keyManager();
    }
    
    /**
     * Konstruktor des Actors Field - Ein K�astchen im Sudokufeld
     * 
     * @param pSudoku Referenz auf das Sudoku Welt Objekt -> f�r Zugriffe auf das Grid
     * @param idField ID eines einzelnen Feld im Sudoku
     * @param gridSize Ma�e des Grids (Standard 9x9)
     * @param clickedImageColor Farbe des Bildes -> wird aus den Einstellungen gelesen
     * @param column Column
     * @param rw Row
     */
    public PlayField(SudokuLogic pSudoku, int idField, int column, int rw, int gridSize, String clickedImageColor) {
        super(pSudoku, idField, column, rw, gridSize, clickedImageColor);
    }
    
    /**
     * Gibt nun immer True zur�ck, da falsche Eingaben m�glich sind
     */
    @Override
    public boolean isValid(int num){
        return true;
    }
}
