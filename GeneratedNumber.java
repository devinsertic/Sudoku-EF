import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot und MouseInfo)

/**
 * Eine Nummer, die dann gesetzt wird, wenn sie durch den Generierungsalgorithmus erstellt wurde, 
 * denn sie muss unveränderlich sein und sich optisch leicht von der normalen Nummer unterscheiden
 * 
 * Erbt von Number
 */
public class GeneratedNumber extends SudokuNumber
{
    /**
     * Act - do whatever the GeneratedNumber wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        forwardToField();
    }
    
    /**
     * Konstruktor
     * 
     * @param number Integer, der als graphische Nummer abgebildet werden soll
     */
    public GeneratedNumber(String number){
        super(number);
    }
    
    /**
     * Setzt die Schriftart
     */
    @Override
    protected void setFont(){
        numberFont = new Font("OptimusPrinceps", true, false , 20);
        color = Color.BLACK;
    }   
}