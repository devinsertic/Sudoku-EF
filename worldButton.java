import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * Ein WorldButton ist eine Art des Buttons, die eine neue Welt erzeugt, den User also weiterleitet
 * 
 */
public class worldButton extends Button
{
    public String designation; //Name des Bildes + Schwierigkeit
    private String worldName; // Name der zu erzeugenden Welt

    public void act(){
        checkHovering(designation);
        checkClick();
    }
    
    public worldButton(String imageName, String wrldName){
        designation = imageName;
        worldName = wrldName;
        setImage(designation+".png");
    }
    
    /**
     * Pr�ft, ob der Button gedr�ckt wurde
     */
    private void checkClick(){
        if(Greenfoot.mouseClicked(this)){
            //change image - wenn image fertig hier erg�nzen
            if(worldName == "Game"){
                // Button ist auf der Titelseite
                click.play();
                SudokuGame world = new SudokuGame(designation);
                world.currentGameDifficulty = designation;
                Greenfoot.setWorld(world);
                
             }
             else if(worldName == "Solver"){
                // Button ist auf der Titelseite
                click.play();
                SudokuSolver world = new SudokuSolver();
                Greenfoot.setWorld(world);
             }
             else if(worldName == "TitleScreen"){
                 clickBack.setVolume(70);
                 clickBack.play();
                 TitleScreen world1 = new TitleScreen();
                 SudokuSolver world = new SudokuSolver();
                 world.isSolvedByAlgorithm = false;
                 Greenfoot.setWorld(world1);
             }
             else {
                 click.play();
             }
         }
    }
    

}
