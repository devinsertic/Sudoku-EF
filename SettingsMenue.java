import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.io.FileWriter;   import java.io.IOException;
import java.io.File;         import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;
/**
 * Im SettingsMenue k�nnen Spieler bestimmte Dinge �ndern, wie die Farbe der Felder
 * oder die Farbe des ausgew�hlten Feldes. Diese Einstellungen werden dann in einer
 * Datei gespeichert und beim Start des Spieles eingelesen
 */
public class SettingsMenue extends World
{
    private String nameSettingsFile = "settings"; // Name der Datei, wo Einstellungen gespeichert werden
    private boolean overwrittenSettings = false; // Reguliert das �berschreiben von dem SettingsFile (Im Kontext vom Settings speichern um Text-�berschneidungen zu vermeiden)
    
    private String[] readSettings = new String[2]; // Eingelesene Einstellungen in Form eines Arrays
    
    /**
     * Konstruktor der SettingsMenue Klasse -> initalisiert UI
     */
    public SettingsMenue()
    {    
        // Welt mit 900*600 pixeln wird erstellt
        super(900, 600, 1);
        prepare();
    }
    
    // Benutzbare Methoden - Hauptmethoden ( public )
    
    /**
     * Hauptmethode, die Helperfunctions aufruft und diese kombiniert um aktuelle Einstellungen
     * zu speichern
     */
    public void saveCurrSettings(){
        List<Setting> allSettings = getObjects(Setting.class); // Holt sich alle anklickbaren Einstellungen
        for(Setting s: allSettings){ // Itteration durch alle Einstellungen
            if(s.isActivated){ // Pr�fen ob die Einstellung aktiviert ist
                writeToFile(nameSettingsFile, s.setting); // Falls ja, speichere diese
            }
        }
        overwrittenSettings = false; // Setzte Attribut wieder auf false, sodass dieses beim n�chsten Speichervorgang wieder m�glich ist die Datei zu aktualisieren
    }
    
    // Helperfunctions ( private )
    
    /**
     * Baut die verschiedenen UI-Elemente des SettingsMenues
     */
    private void prepare(){
        // Zur�ck-Button
        worldButton button = new worldButton("back","TitleScreen");
        addObject(button, 75, 550);
        
        // Titel: Einstellungen
        GreenfootImage titel = new GreenfootImage("Einstellungen.png");
        getBackground().drawImage(titel, getWidth()/2- 150, 20);
        
        // Label: Markiertes Feld
        GreenfootImage field = new GreenfootImage("Field.png");
        getBackground().drawImage(field, getWidth()/2 - 370, 150);
        
        // Farb Optionen - Markiertes Feld
        GreenfootImage greenField = new GreenfootImage("filled-green.png");
        addObject(new Setting(greenField, "Field&green", 1),getWidth()/2, 170);
        
        GreenfootImage yellowField = new GreenfootImage("filled-yellow.png");
        addObject(new Setting(yellowField, "Field&yellow", 1),getWidth()/2 +80, 170);
        
        GreenfootImage redField = new GreenfootImage("filled-red.png");
        addObject(new Setting(redField, "Field&red", 1),getWidth()/2 +160, 170);
        
        GreenfootImage grayField = new GreenfootImage("filled-grey.png");
        addObject(new Setting(grayField, "Field&grey", 1),getWidth()/2+240, 170);
        
        
        //Label: Umrandung
        GreenfootImage border = new GreenfootImage("Umrandung.png");
        getBackground().drawImage(border, getWidth()/2 - 360, 250);
        
        // Farb Optionen - Grid Umrandung
        
        GreenfootImage orangeColor = new GreenfootImage(50,50);
        orangeColor.setColor(Color.ORANGE);
        orangeColor.fillRect(0, 0, 40, 40);
        addObject(new Setting(orangeColor, "Border&orange", 2),getWidth()/2+45, 275);
        
        GreenfootImage redColor = new GreenfootImage(50,50);
        redColor.setColor(Color.RED);
        redColor.fillRect(0, 0, 40, 40);
        addObject(new Setting(redColor, "Border&red", 2),getWidth()/2+125, 275);
        
        GreenfootImage cyanColor = new GreenfootImage(50,50);
        cyanColor.setColor(Color.CYAN);
        cyanColor.fillRect(0, 0, 40, 40);
        addObject(new Setting(cyanColor, "Border&cyan", 2),getWidth()/2+205, 275);
        
        GreenfootImage blackColor = new GreenfootImage(50,50);
        blackColor.setColor(Color.BLACK);
        blackColor.fillRect(0, 0, 40, 40);
        addObject(new Setting(blackColor, "Border&black", 2),getWidth()/2+285, 275);
        
        
        // Option: Zahlenarten
        // sp�ter
        
        // Speicher Button
        functionButton save = new functionButton("save", "save");
        addObject(save, getWidth()/2, 545);
        
        setSettingsFromFile();
    }
    
    // Methoden zum Updaten und auslesen der Settings
    
    /**
     * Schreibt eine �bergebene Einstellung in eine �bergebene Datei
     * 
     * @param fileName Name der Datei, in welche Einstellungen reingeschrieben werden sollen
     * @param setting Einstellung, die der in die Datei gespeichert/geschrieben werden soll
     */
    private void writeToFile(String fileName, String setting){
        try{
            if(!overwrittenSettings){ // Um l�schungen bei mehreren Einstellungen, die zur gleichen Zeit vorgenommen werden zu verhindern
                new FileWriter(nameSettingsFile, false).close(); // Um File am anfang zu l�schen
                overwrittenSettings = true;
            }
            FileWriter settingsWriter = new FileWriter(fileName,true); // Neue FileWriter um in die Datei schreiben zu k�nnen
            settingsWriter.append(setting); // schreibe Einstellung in Datei
            settingsWriter.append("\n"); // F�ge einen Zeilenumbruch hinzu
            settingsWriter.close(); // schlie�e die Datei wieder
        }
        catch (IOException e){ // F�ngt m�gliche Fehler ab, die passieren k�nne und gibt diese aus
            e.printStackTrace();
        }
    }
    
    /**
     * Lie�t Einstellungen aus einer Datei aus und speichert diese in dem Attribut loadedSetings f�r weitere Operationen
     */
    private void setSettingsFromFile(){
        FileReader fileReader = new FileReader(nameSettingsFile); //Neuer FileReader um die Datei lesen zu k�nnen
        readSettings = fileReader.readFile(); // Lie�t die Datei ein
        selectSettings(); // Setzt gespeicherte Einstellungen standartm��ig auf aktiviert
    }
    
    /**
     * Setzt beim letzten Speichern gespeicherte Einstellungen, standartm��ig, beim Aufrufen der Einstellungen, auf aktiviert
     */
    private void selectSettings(){
        List<Setting> allSettings = getObjects(Setting.class); // Holt sich alle anklickbaren Einstellungen
        for(Setting setting : allSettings){ // Iterriert durch diese
            if(setting.setting.equals(readSettings[0]) || setting.setting.equals(readSettings[1])){ //Pr�ft ob die Einstellungsm�glichkeit der aktiverten aus der gelesenen Datei entspricht
                setting.checkActivated(); // Wenn ja, setzte alle anderen auf nicht aktiviert
                setting.activate(); // Und aktivieren diese Einstellung
            }
        }
    }
}
