import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * Nummer, die sich auf der UI, genauer gesagt im Sudoku auf/in einem Feld 
 */
public class SudokuNumber extends Actor
{
    private int fontSize = 22;
    private int fieldSize = 40; // Gr��e des Feldes in px
    private int[] corNumber = {15, 27}; // fontsize 20 points = 27 px (height,y) -> x = 15 durch ausprobieren -> Koordinaten innerhalb eines Feldes
    
    public Font numberFont; // Schriftart der Zahl
    public Color color; // Farbe der Zahl
    
    public void act(){
        forwardToField();
    }
    
    /**
     * Konstruktor
     * 
     * @param number Integer, der als graphische Nummer abgebildet werden soll
     */
    public SudokuNumber(String number){
        setFont();
        printNum(number);
    }
    
    /**
     * Positioniert und zeichnet die Zahl auf die Welt
     */
    private void printNum(String number){
        GreenfootImage numb = new GreenfootImage(fieldSize, fieldSize);
        numb.setColor(color);
        numb.setFont(numberFont);
        
        numb.drawString(number, corNumber[0], corNumber[1]);
        setImage(numb);
    }
    
    /**
     * Leitet eine Klick auf die Zahl, an das Feld, indem sie sich befindet, weiter.
     * Denn sie blockt das Feld
     */
    public void forwardToField(){
        if(Greenfoot.mouseClicked(this)){
            List<Field> currFields = getWorld().getObjectsAt(getX()-20, getY()-20, Field.class);
            for(Field f : currFields){
                f.clickManager(true);
            }
        }
    }
    
    /**
     * Setzt die Schriftart
     */
    protected void setFont(){
        numberFont = new Font("OptimusPrinceps", false, true , fontSize);
        color = Color.BLUE;
    }
}
