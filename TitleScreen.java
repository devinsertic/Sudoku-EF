import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Titelscreen des Spieles
 * 
 * @author Devin S. & Bene R. 
 */
public class TitleScreen extends World
{
    private String[] difficultyLevels = {"easy", "middle", "hard", "hardcore"}; // Auswahl der Spiel-Schwierigkeiten
    private int spaceBetweenworldButtons = 245; // Platz zwischen der Button
    private String titelScreen = "Titelbild2.png"; // Titelbild
    
    
    /**
     * Titelbildschirm - Konstruktor
     * 
     */
    public TitleScreen()
    {    
        // 900 x 600 pixel Welt
        super(900, 600, 1);
        prepare(); // Hinzuf�gen der Elemente
    }

    /**
     * Bereitet den TitelBildschirm vor
     * -> Erstellt die UI des Titelbildschirmes 
     */
    private void prepare()
    {
        GreenfootImage titel = new GreenfootImage(titelScreen); // Titelbilschirm wird erstellt
        getBackground().drawImage(titel, getWidth()/2 - 100, 20); // und hinzugef�gt
        
        Settings settings = new Settings(); // Einstellungsmen�knopf wird erstellt
        addObject(settings,855,36); // und hinzugef�gt
        
        InfoButton info = new InfoButton();
        addObject(info, 780, 36);
        
        
        int buttonSet = 0; // counter f�r Kn�pfe die bereits in die Welt gesetzt wurden
        for(String s : difficultyLevels){ // Itterration �ber alle Schwierigkeitsstuffen
            if(s == "hardcore") {
                continue;
            }
            worldButton button = new worldButton(s, "Game"); // Erstellt neuen WorldButton
            addObject(button, 200+buttonSet*spaceBetweenworldButtons, 200);
            buttonSet++; // Incrementiert counter f�r gesetzte Kn�pfe
        }
        
        worldButton button = new worldButton("Solver", "Solver"); // Knopfen um zu Solver zu gelangen
        addObject(button, 450, 500);
        
        worldButton hardcoreMode = new worldButton(difficultyLevels[3], "Game"); // Knopfen um zu Solver zu gelangen
        addObject(hardcoreMode, 444, 300);
    }
}