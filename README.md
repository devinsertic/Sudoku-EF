# Sudoku EF

## Elemente

Eine Sudoku Programm in Java mit Greenfoot.
- Sudoku's generieren (Algorithmus)
- Sudoku's lösen (Algorithmus)

## Quellen

- Erklärung, wie ein Solver und ein Generator funktionieren -> https://www.kompf.de/sudoku/algo.html

- Sudoku Generator Idee für eine Vorgehensweise -> https://praxistipps.chip.de/sudoku-selber-erstellen-zahlenraetsel-von-hand-mit-dem-pc_114316

- Grundlagen Backtracking -> https://www.betriebswirtschaft-lernen.net/erklaerung/backtracking/

- Grundlagen Recursion -> https://www.w3schools.com/java/java_recursion.asp, https://www.javatpoint.com/recursion-in-java
