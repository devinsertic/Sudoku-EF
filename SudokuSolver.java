import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Welt, in der man ein Sudoku eintippen kann und dieses gel�st wird -> Sudokul�ser
 * 
 * @author Devin S. & Bene R.
 */
public class SudokuSolver extends SudokuLogic
{
    /**
     * Konstruktor SudokuSolver - baut die Welt
     * 
     */
    public SudokuSolver()
    {
        super();
        prepare(); // Aufbau der Welt - Hinzuf�gen der Elemente
    }
    
    /**
     *  
     *  Baut das Sudokufeld - UI
     * 
     */
    private void prepare(){
        // Erkl�rungsnachricht
        getBackground().setColor(Color.BLACK); // Setzt hintergrund Schriftfarbe der obenstehenden Nachticht auf Schwarz
        getBackground().setFont(new Font("OptimusPrinceps", false, false , 17)); // Setzt Schriftart der Nachricht
        getBackground().drawString("Im Netz die gegebenen Wert deines Sudokus eintragen und unten auf l�sen klicken", 150, 80); // F�gt die Nachricht der Welt hinzu
        
        // Solve
        functionButton button1 = new functionButton("solve","solve"); // Funktionsbutton um �bergebenes Sudoku zu l�sen
        addObject(button1,getWidth()/2, 550); // f�gt button der Welt hinzu
        
        // Sudokufeld
        buildGrid(getWidth()/2 - imageSize*(squareSize/2), getHeight()/2 - imageSize*(squareSize/2), SudokuLogic.SOLVER); //Baut das Sudoku-Netz
        setColoredGridBorder(); // Baut Sudokuumrandung
        
        // Buttons
        // Solve
        worldButton button = new worldButton("back","TitleScreen"); // Zur�ckbutton
        addObject(button, 75, 550); // f�gt zur�ckbutton der Welt hinzu
        // Delete
        functionButton button2 = new functionButton("clearGrid", "Delete");
        addObject(button2, 800 , 290);
        // Delete String
        getBackground().setFont(new Font("OptimusPrinceps", false, false , 17)); // Setzt Schriftart der Nachricht
        getBackground().drawString("L�schen", 770, 335);
    }
}
