import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * Ein Button, der benutzt wird um Funktionen auszuf�hren innerhalb einer Welt
 */
public class functionButton extends Button
{
    private String function;
    public String designation;
    /**
     *                                                                                                                                                                              
     */
    public void act() 
    {
        checkHovering(designation);
        if(Greenfoot.mouseClicked(this)){
            click.play();
            doFunction();
        }
    }   
    
    /**
     * Konstruktor
     * 
     * @param functionName dient dazu einem Button eine funktion zuzuweisen
     * @param imageName Name des Button Bildes
     */
    public functionButton(String functionName, String imageName){
        function = functionName;
        designation = imageName;
        setImage(designation+".png");
    }
            
    /**
     * F�hrt die durch functionName zugewiesenen Funktionen aus
     */
    public void doFunction(){
        if(function == "solve"){
            SudokuSolver world = (SudokuSolver) getWorld();
            world.printSolvedGridOnUI();
        }
        else if(function == "save"){
            SettingsMenue settings = (SettingsMenue) getWorld();
            settings.saveCurrSettings();
        }
        else if(function == "clearGrid") {
            SudokuSolver world = (SudokuSolver) getWorld();
            world.clearSudoku();
        }
                // SudokuGame
        else if(function == "solveGameGrid"){ // L�st das aktuelle Sudoku, welches der Nutzer angezeigt bekommt
            SudokuGame world = (SudokuGame) getWorld();
            world.solvePlayerGrid();

        }
        else if(function == "clearGameGrid") { // L�scht alle Nutzereingaben aus dem Sudoku
            trash.play();
            SudokuGame world = (SudokuGame) getWorld();
            world.resetSudoku();
        }
        else if(function == "checkGameGrid") { // �berpr�ft alle Nutzereingaben
            SudokuGame world = (SudokuGame) getWorld();
            world.checkWrong();
        }
        else if(function == "newSudoku") { // Generiert ein neues Sudoku, f�r den Benutzer
            SudokuGame world = (SudokuGame) getWorld();
            world.isSolvedByAlgorithm = false;
            world.clearSudoku();
            List<Field> allFields = getWorld().getObjects(Field.class);
            for(Field d: allFields) {
                d.undoFieldWrong();
            }
            world.generateGrid(world.getDifficultyValue(world.currentGameDifficulty));
            world.setRightAttributNumberInField();
        }
    }
}
