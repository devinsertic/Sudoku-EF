import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot und MouseInfo)

/**
 * Erg�nzen Sie hier eine Beschreibung f�r die Klasse hoverString.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class hoverString extends Actor
    {
    private int fontSize = 20;
    private Font numberFont = new Font("OptimusPrinceps", false, false , fontSize);
    private Color color = Color.BLACK;
    /**
     * Act - tut, was auch immer hoverString tun will. Diese Methode wird aufgerufen, 
     * sobald der 'Act' oder 'Run' Button in der Umgebung angeklickt werden. 
     */
    public void act() 
    {
        // Erg�nzen Sie Ihren Quelltext hier...
    }
    
    public hoverString(String text){
        printNum(text);
    }
    
    private void printNum(String text){
        GreenfootImage numb = new GreenfootImage(50, 50);
        numb.setColor(color);
        numb.setFont(numberFont);
        
        numb.drawString(text, 15, 27);
        setImage(numb);
    }
}