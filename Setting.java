import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * Ein Setting ist eine Einstellungsmöglichkeit/Einstellungsoption innerhalb des SettingsMenue
 */
public class Setting extends Actor
{
    public boolean isActivated = false; // Status -> aktiviert oder nicht
    public String setting; // Name des Settings 
    private GreenfootImage actorImage; // Bild des Settings aus SettingsMenue
    private int row;
    
    /**
     * Konstruktor
     * 
     * @param image Bild der Einstellungsmöglichkeit/Einstellungsoption
     * @param pSetting Name des Settings / der Einstellungsoption
     * @param prow optische Reihe innerhalb SettingsMenue
     */
    public Setting(GreenfootImage image, String pSetting, int prow){
        setting = pSetting;
        actorImage = image;
        row = prow;
        setImage(image);
    }
    
    /**
     * Act - do whatever the Setting wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        checkClick();
    }
    
    /**
     * Prüft, ob die Einstellungsoption geklickt wurde
     */
    private void checkClick(){
        if(Greenfoot.mouseClicked(this)){
            checkActivated();
            activate();
        }
    }
    
    /**
     * Wenn etwas neues aktiviert wurde, wird alles in der selben Reihe deaktiviert -> Es kann in einer Kategorie nur eine Einstellungsoption ausgewählt sein
     */
    public void checkActivated(){
        List<Setting> allSettings = getWorld().getObjects(Setting.class);
        for(Setting s : allSettings){
            if(s.row == row){
                s.uncheck();
            }
        }
    }
    
    /**
     * Deaktiviert eine Einstellungsoption
     */
    public void uncheck(){
        isActivated = false;
        if(row == 2){
            actorImage.scale(50, 50);
            setImage(actorImage);
        }
        else{
            actorImage.scale(40, 40);
            setImage(actorImage);
        }
    }
    
    /**
     * Aktiviert eine Einstellungsoption
     */
    public void activate(){
        isActivated = true;
        if(row == 2){
            actorImage.scale(68, 68);
            setImage(actorImage);
        }
        else{
            actorImage.scale(55, 55);
            setImage(actorImage);
        }
    }
}
