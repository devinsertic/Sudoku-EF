import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * Feld eines Sudokus in der UI -> Repr�sentation eines Feldes im grid
 */
public class Field extends Actor
{
    public final int id; // ID des Feldes
    public final int col; // Spalte des Feldes 
    public final int row; // Reihe des Feldes
    public final int size = 40; // Gr��e des Feldes (Bild) in px
    
    
    
    public int number = 0; // Number, die sich in dem Feld befindet
    
    private SudokuLogic sudoku; // Ganzes Sudoku Objekt -> f�r gewisse Funktionen
    
    private  boolean numChanged = false; // Ob die Nummer in dem Feld bereits ge�ndert wurde

    private int speedArrawMove = 15; // Schnelligkeit f�r Bewegung mit Pfeiltasten - Desto kleiner desto schneller

    private boolean clicked = false; // Ob das Felder gecklickt wurde
    private String clickedImage; // Bild bzw. Farbe, welche gesetzt wird, wenn das Feld gecklickt wurde
    private int allowChars; // Nummer die eingegeben werden d�rfen - bei 9x9 von 1-9 ...
    
    private boolean firstClickedField = false; // Zum sezten des ersten angeklickten Feldes
    
    public boolean containsUserNum = false; // Zeigt, ob sich eine eingegebene Zahl in dem Feld befindet, oder eine generierte
    
    public boolean isWrong = false; // Zeigt, ob die Zahl im Feld falsch ist (nach �berpr�fung)
    
    /**
     * Act - Schritt eines Feldes - pr�ft eingaben und interaktion
     */
    public void act() 
    {   
        clickManager(false); // 
        setGetNumber(0);
        keyManager();
    }
    
    /**
     * Konstruktor des Actors Field - Ein K�astchen im Sudokufeld
     * 
     * @param pSudoku Referenz auf das Sudoku Welt Objekt -> f�r Zugriffe auf das Grid
     * @param idField ID eines einzelnen Feld im Sudoku
     * @param gridSize Ma�e des Grids (Standard 9x9)
     * @param clickedImageColor Farbe des Bildes -> wird aus den Einstellungen gelesen
     * @param column Column
     * @param rw Row
     */
    public Field(SudokuLogic pSudoku, int idField, int column, int rw, int gridSize, String clickedImageColor){
        sudoku = pSudoku;
        id = idField+1;
        col = column+1;
        row = rw+1;
        clickedImage = clickedImageColor;
        allowChars = gridSize;
    }
    
    // Methoden zur Ver�nderung des Aussehens und somit auch grundlage f�r sp�teres Verhalten
    /**
     * Setzt ein Feld auf geklickt
     */
    public void setClicked(){
        setImage(clickedImage);
        clicked = true;
    }

    /**
     * Setzt das Feld wieder auf ungeklicked
     */
    public void setUnClicked(){
        if(isWrong){
            setImage("wrong.png");
            clicked = false;
        }
        else{
            setImage("40px - grau.png");
            clicked = false;
        }
    }

    /**
     * Setzt das Feld als Wrong und �ndert das Bild
     */
    public void setFieldWrong() {
        setImage("wrong.png");
        isWrong = true;
    }
    
    /**
     * �nder das Feld Gr�n, wenn die �berpr�fung erfolgreich war und der Spieler somit gewonnen hat
     */
    public void setFieldVictory() {
        setImage("filled-green.png");
    }
    
    /**
     * Setzt die gr�nen Victory-Felder wieder in den Ursprungszustand
     */
    public void unsetFieldVictory() {
        setImage("40px - grau.png");
    }
    
    /**
     * Setzt die als Falsch markierten Felder in den Ursprungszustand
     */
    public void undoFieldWrong() {
        setImage("40px - grau.png");
        isWrong = false;
    }
    
    //
    
    public void setContainsUserNum(boolean val) {
        containsUserNum = val;
    }
    
    // Eigentliche Main-Methoden des Feldes
    /**
     * Pr�ft, ob das Feld geklickt wird
     * 
     * @param accesThroughNumber Sorgt daf�r, dass wenn eine Nummer auf dem Feld ist, die es unm�glich macht das Feld zu anzuklicken, diese Klick trotzdem registriert wird durch die Nummer die geklickt wird (Weiterleitung des Klicks) 
     */
    public void clickManager(boolean accesThroughNumber){
        if(Greenfoot.mouseClicked(this) || accesThroughNumber){
            List<Field> allFields = getWorld().getObjects(Field.class);
            for(Field e : allFields){
                if(e.clicked){
                    e.setUnClicked();
                }
            }
            setClicked();
        }
    }
    
    /**
     * Nimmt die eingegebene Nummer entegen und setzt diese sowohl in die UI, als auch ins grid
     * 
     * @param val Sorgt daf�r, dass bestimmte Schritte �bersprungen werden k�nnen, falls die Zahl durch einen Algorithmus gesetzt wird (braucht nur UI und nicht grid)
     */
    public void setGetNumber(int val){ //Parameter only for algorithm
        if(val != 0){
            number = val;
            GeneratedNumber input = new GeneratedNumber(String.valueOf(val));
            getWorld().addObject(input, getX(), getY());
        }
        else if(clicked){
            if(getWorld().getObjectsAt(getX(), getY(), GeneratedNumber.class).isEmpty()) {
                String lastChar = Greenfoot.getKey();
                try{
                    int num = Integer.parseInt(lastChar);
                    if (isValid(num)){
                        //System.out.println(num);
                        SudokuNumber input = new SudokuNumber(lastChar);
                        numChanged = true;
                        if(number != num || numChanged){
                            rmNumber(getX(), getY());
                        }
                        number = num;
                        containsUserNum = true;
                        if(isWrong) {
                            undoFieldWrong();
                            isWrong = false;
                        }
                        getWorld().addObject(input, getX(), getY());
                        sudoku.setArray(row-1, col-1, num);
                        System.out.println(sudoku.getArray()[row-1][col-1]);
                        System.out.println("printed");
                    }
                } catch (Exception e){
                    return;
                }
            }

        }
    }
    
    /**
     * Pr�ft, ob die Nummer, nach den Sudoku Regeln, erlaubt ist einzugeben.
     * Falls nein, wird die Eingabe unterbunden
     * 
     * @param num zu �berpr�fende Nummer
     */
    public boolean isValid(int num) {
        if (num > allowChars) return false;
        if (num <= 0) return false;
        if(!sudoku.isSafe(row-1, col-1, num)){
            return false;
        }
       
        return true;
    }
    
    /**
     * Entfernt Nummer aus der UI und aus dem Grid
     * 
     * @param x X-Koordinate des Feldes, worin sich die Zahl befinden
     * @param y Y-Koordinate des Feldes, worin sich die Zahl befinden 
     */
    public void rmNumber(int x, int y){
        if(getWorld().getObjectsAt(getX(), getY(), GeneratedNumber.class).isEmpty()) {
            containsUserNum = false;
            number = 0;
            List<SudokuNumber> currNum = getWorld().getObjectsAt(x-20, y-20, SudokuNumber.class);
            for(SudokuNumber e : currNum){
                getWorld().removeObject(e);
                sudoku.setArray(row-1, col-1, 0);
            }    
        }

    }
 
    /**
     * Wartet auf bestimmte Tastatur-Eingaben und verarbeitet diese dann 
     */
    public void keyManager(){
        // Erstes geklicktes Feld
        if(!firstClickedField) {
            moveThroughGrid(5, 5);
            firstClickedField = true;
        }
        // L�schung von Zahlen
        if(Greenfoot.isKeyDown("backspace") && number != 0 && clicked){
            rmNumber(getX(), getY());
            return;
        }
        // Eine y-Koordinate im Grid nach oben
        else if(Greenfoot.isKeyDown("up") && clicked){
            moveThroughGrid(row-1, col);
            Greenfoot.delay(speedArrawMove);
            return;
        }
        // Eine y-Koordinate im Grid nach unten
        else if(Greenfoot.isKeyDown("down") && clicked){
            moveThroughGrid(row+1, col);
            Greenfoot.delay(speedArrawMove);
            return;
        }
        // Einen Schritt im grid nach links
        else if(Greenfoot.isKeyDown("left") && clicked){
            moveThroughGrid(row, col-1);
            Greenfoot.delay(speedArrawMove);
            return;
        }
        // Einen Schritt im grid nach rechts
        else if(Greenfoot.isKeyDown("right") && clicked){
            moveThroughGrid(row, col+1);
            Greenfoot.delay(speedArrawMove);
            return;
        }
    }

    
    
    /**
     * Pfeiltasten Navigation durch das Sudoku
     * 
     * @param rowp ge�nderte Row
     * @param column ge�nderte Column
     */
    private void moveThroughGrid(int rowp, int column){
        List<Field> allFields = getWorld().getObjects(Field.class);
        for(Field f : allFields){
            if(f.row == rowp && f.col == column){
                f.clickManager(true);
            }
        }
    }
}