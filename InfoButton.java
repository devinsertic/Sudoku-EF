import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot und MouseInfo)

/**
 * Ein Button, der die Hilfeseite �ffnet
 */
public class InfoButton extends Button
{
    /**
     * Act - tut, was auch immer InforButton tun will. Diese Methode wird aufgerufen, 
     * sobald der 'Act' oder 'Run' Button in der Umgebung angeklickt werden. 
     */
    public void act() 
    {
        // Erg�nzen Sie Ihren Quelltext hier...
        checkClick();
    }
    
    public InfoButton()
    {
        GreenfootImage image = getImage();
        image.scale(image.getWidth() - 20, image.getHeight() - 20);
        setImage(image);
    }
    
    /**
     * Pr�ft, ob das Objekt geklickt
     */
    private void checkClick(){
        if(Greenfoot.mouseClicked(this)){
            // Settings �ffnen
            Rules world = new Rules();
            Greenfoot.setWorld(world);
        }
    }


}