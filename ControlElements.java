import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ControlElements here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ControlElements extends Actor
    {
    private boolean mouseOver = false;

    /**
     * Act - do whatever the ControlElements wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }
    
    public void checkHovering(String designation){
        if(!mouseOver && Greenfoot.mouseMoved(this)){
            setImage(designation + "-hover.png");
            mouseOver = true;
        }
        if(mouseOver && Greenfoot.mouseMoved(null) && !Greenfoot.mouseMoved(this)){
            setImage(designation + ".png");
            mouseOver = false;
        }
    }
}
